-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: dd49628
-- Erstellungszeit: 01. Mrz 2021 um 11:16
-- Server-Version: 10.5.6-MariaDB-1:10.5.6+maria~focal-log
-- PHP-Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `d032aa29`
--
CREATE DATABASE IF NOT EXISTS `d032aa29` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `d032aa29`;

DELIMITER $$
--
-- Funktionen
--
DROP FUNCTION IF EXISTS `FIRST_DAY_OF_WEEK`$$
CREATE DEFINER=`d032aa29`@`85.13.128.8` FUNCTION `FIRST_DAY_OF_WEEK` (`day` DATE) RETURNS DATE NO SQL
BEGIN
  RETURN SUBDATE(day, WEEKDAY(day));
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `appointments`
--

DROP TABLE IF EXISTS `appointments`;
CREATE TABLE `appointments` (
  `appsid` int(11) NOT NULL,
  `appdatetime` datetime NOT NULL DEFAULT current_timestamp(),
  `appduration` int(11) NOT NULL DEFAULT 15,
  `appsubject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `appbody` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `appparticipants` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `apptimezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appseverity` int(11) DEFAULT NULL,
  `appsource` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appcredat` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `authID`
--

DROP TABLE IF EXISTS `authID`;
CREATE TABLE `authID` (
  `autsid` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `autcomment` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `authIDcheck`
-- (Siehe unten für die tatsächliche Ansicht)
--
DROP VIEW IF EXISTS `authIDcheck`;
CREATE TABLE `authIDcheck` (
`autsid` varchar(128)
,`sha2` varchar(64)
,`sha2sha2` varchar(64)
,`autcommect` varchar(200)
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bundy`
--

DROP TABLE IF EXISTS `bundy`;
CREATE TABLE `bundy` (
  `buntime` datetime NOT NULL DEFAULT current_timestamp(),
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `bundydatesum`
-- (Siehe unten für die tatsächliche Ansicht)
--
DROP VIEW IF EXISTS `bundydatesum`;
CREATE TABLE `bundydatesum` (
`datdate` date
,`dathours` decimal(41,0)
,`datminutes` decimal(44,0)
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bundytimes`
--

DROP TABLE IF EXISTS `bundytimes`;
CREATE TABLE `bundytimes` (
  `buncome` datetime NOT NULL DEFAULT current_timestamp(),
  `bunleave` datetime DEFAULT NULL,
  `buninterval` datetime DEFAULT NULL,
  `bunsource` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buncomment` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `bundywithdiff`
-- (Siehe unten für die tatsächliche Ansicht)
--
DROP VIEW IF EXISTS `bundywithdiff`;
CREATE TABLE `bundywithdiff` (
`buncome` datetime
,`bunleave` datetime /* mariadb-5.3 */
,`bundiff` bigint(17)
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `currStatus`
--

DROP TABLE IF EXISTS `currStatus`;
CREATE TABLE `currStatus` (
  `cstset` datetime NOT NULL DEFAULT current_timestamp(),
  `cststatusb64` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cstdesc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cstsource` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `csthash` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur des Views `authIDcheck`
--
DROP TABLE IF EXISTS `authIDcheck`;

DROP VIEW IF EXISTS `authIDcheck`;
CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `authIDcheck`  AS SELECT `authID`.`autsid` AS `autsid`, sha2(`authID`.`autcomment`,256) AS `sha2`, sha2(sha2(`authID`.`autcomment`,256),256) AS `sha2sha2`, `authID`.`autcomment` AS `autcommect` FROM `authID` ;

-- --------------------------------------------------------

--
-- Struktur des Views `bundydatesum`
--
DROP TABLE IF EXISTS `bundydatesum`;

DROP VIEW IF EXISTS `bundydatesum`;
CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `bundydatesum`  AS SELECT cast(`bundywithdiff`.`buncome` as date) AS `datdate`, floor(sum(`bundywithdiff`.`bundiff`) / 60 / 60) AS `dathours`, floor(sum(`bundywithdiff`.`bundiff`) / 60) - floor(sum(`bundywithdiff`.`bundiff`) / 60 / 60) * 60 AS `datminutes` FROM `bundywithdiff` GROUP BY cast(`bundywithdiff`.`buncome` as date) ;

-- --------------------------------------------------------

--
-- Struktur des Views `bundywithdiff`
--
DROP TABLE IF EXISTS `bundywithdiff`;

DROP VIEW IF EXISTS `bundywithdiff`;
CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `bundywithdiff`  AS SELECT `bundytimes`.`buncome` AS `buncome`, coalesce(`bundytimes`.`bunleave`,coalesce(`bundytimes`.`buninterval`,current_timestamp())) AS `bunleave`, time_to_sec(timediff(coalesce(`bundytimes`.`bunleave`,coalesce(`bundytimes`.`buninterval`,current_timestamp())),`bundytimes`.`buncome`)) AS `bundiff` FROM `bundytimes` ;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`appsid`),
  ADD UNIQUE KEY `i1_datesubject` (`appdatetime`,`appsubject`);

--
-- Indizes für die Tabelle `authID`
--
ALTER TABLE `authID`
  ADD PRIMARY KEY (`autsid`);

--
-- Indizes für die Tabelle `bundy`
--
ALTER TABLE `bundy`
  ADD PRIMARY KEY (`buntime`);

--
-- Indizes für die Tabelle `bundytimes`
--
ALTER TABLE `bundytimes`
  ADD UNIQUE KEY `i0_bundytimes` (`buncome`,`bunleave`);

--
-- Indizes für die Tabelle `currStatus`
--
ALTER TABLE `currStatus`
  ADD PRIMARY KEY (`cstset`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `appointments`
--
ALTER TABLE `appointments`
  MODIFY `appsid` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
