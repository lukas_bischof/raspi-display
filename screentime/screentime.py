import xmlrpc.client
import sys
import hashlib
import configparser
import os
import datetime
import ssl

def writeConfig(configObj,fileString):
    with open(fileString, 'w') as configfile:
        configObj.write(configfile)

def initConfig(settingsfile):
    Config=configparser.ConfigParser()
    Config.read(settingsfile)
    if(not Config.has_section("general")):
        Config.add_section("general")
        writeConfig(Config,settingsfile)
    if(not Config.has_option("general", "auth")):
        Config["general"]["auth"]="<enter authID>"
        writeConfig(Config,settingsfile)
    if(not Config.has_option("general", "rpc_uri")):
        Config["general"]["rpc_uri"]="rpc_uri"
        writeConfig(Config,settingsfile)
    return Config

def setOption(configObj,settingsfile,section,key,value):
    if(not configObj.has_section(section)):
        configObj.add_section(section)
    Config[section][key]=value
    writeConfig(configObj,settingsfile)

if __name__ == "__main__":
    settingsfile = os.path.dirname(os.path.realpath(__file__))+os.path.sep+"settings.ini"
    Config = initConfig(settingsfile)
    authID = Config.get("general", "auth")
    rpc_uri = Config.get("general", "rpc_uri")

    sslContext = ssl.SSLContext()
    with xmlrpc.client.ServerProxy(rpc_uri,context=sslContext) as proxy:
        returnval=proxy.bundy.check(sys.argv[1],str(hashlib.sha256(authID.encode('utf-8')).hexdigest()))
        print(returnval)
    setOption(Config,settingsfile,"misc","lastrun",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
