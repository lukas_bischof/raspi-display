#!/usr/bin/env python3

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow,QLabel,QPushButton,QTableWidget,QGraphicsWidget,QTableWidgetItem
from PyQt5.QtCore import Qt,QTimer
from PyQt5 import uic,QtGui
from datetime import datetime
import requests
import xmlrpc.client
import datetime
import json
import hashlib
import base64
from PIL import Image
import io
import os
import configparser
from pushbullet import Pushbullet

# Subclass QMainWindow to customise your application's main window
class MainWindow(QMainWindow):
    settingsfile = os.path.dirname(os.path.realpath(__file__))+os.path.sep+"settings.ini"
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        
        self.initConfig()

        uic.loadUi(os.path.dirname(os.path.realpath(__file__))+os.path.sep+"mainwindow.ui", self) # Load the .ui file

        self.buzzerButton = self.findChild(QPushButton, 'buzzerButton')
        self.appointmentTable = self.findChild(QTableWidget, 'appointmentsTable')
        self.statusLabel = self.findChild(QLabel, 'statusLabel')
        self.stundenLabel = self.findChild(QLabel, 'stundenLabel')
        self.timeLabel = self.findChild(QLabel, 'timeLabel')
        self.feedbackLabel = self.findChild(QLabel, 'feedbackLabel')

        self.buzzerButton.clicked.connect(lambda:self.pressButton(self.buzzerButton))
        self.setWindowFlags(Qt.Window|Qt.FramelessWindowHint)

        self.appointmentTable.verticalHeader().setVisible(False)
        
        self.timer = QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.data_update)
        self.timer.start()

        self.clockTimer = QTimer()
        self.clockTimer.setInterval(1000)
        self.clockTimer.timeout.connect(self.writeClock)
        self.clockTimer.start()

        self.move(0, 0)

        self.show()
    
    def writeConfig(self):
        with open(self.settingsfile, 'w') as configfile:
            self.Config.write(configfile)

    def initConfig(self):
        self.Config=configparser.ConfigParser()
        self.Config.read(self.settingsfile)
        if(not self.Config.has_section("general")):
            self.Config.add_section("general")
            self.writeConfig()
        if(not self.Config.has_option("general", "auth")):
            self.Config["general"]["auth"]="<enter authID>"
            self.writeConfig()
        if(not self.Config.has_option("general", "rpc_uri")):
            self.Config["general"]["rpc_uri"]="rpc_uri"
            self.writeConfig()
        if(not self.Config.has_section("debug")):
            self.Config.add_section("debug")
            self.writeConfig()
        if(not self.Config.has_option("debug", "debugmode")):
            self.Config["debug"]["debugmode"]="False"
            self.writeConfig()
        if(not self.Config.has_section("pushbullet")):
            self.Config.add_section("pushbullet")
            self.writeConfig()
        if(not self.Config.has_option("pushbullet", "apikey")):
            self.Config["pushbullet"]["apikey"]="enter_api_key"
            self.writeConfig()
        if(not self.Config.has_option("pushbullet", "title")):
            self.Config["pushbullet"]["title"]="enter_title"
            self.writeConfig()
        if(not self.Config.has_option("pushbullet", "text")):
            self.Config["pushbullet"]["text"]="enter_text"
            self.writeConfig()

    def setOption(self,section,key,value):
        if(not self.Config.has_section(section)):
            self.Config.add_section(section)
        self.Config[section][key]=value
        self.writeConfig()

    def reenableButton(self):
        self.buzzerButton.setDisabled(False)
        self.feedbackLabel.setVisible(False)

    def pressButton(self,button):
        self.buzzerButton.setEnabled(False)
        self.feedbackLabel.setVisible(True)

        pb_apikey=self.Config["pushbullet"]["apikey"]
        pbtitle=self.Config["pushbullet"]["title"]
        pbtext=self.Config["pushbullet"]["text"]

        pb = Pushbullet(pb_apikey)
        pb.push_note(pbtitle,pbtext)

        QTimer.singleShot(5000, self.reenableButton)
        
        if self.Config["debug"]["debugmode"]=="True":
            QApplication.instance().quit()

    def writeClock(self):
        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M")
        self.timeLabel.setText(current_time)

    def data_update(self):
        self.timer.setInterval(15000)
        self.appointmentTable.setColumnCount(3)
        self.appointmentTable.setHorizontalHeaderLabels("Zeit;Dauer;Subject".split(";"))

        try:
            with xmlrpc.client.ServerProxy(self.Config["general"]["rpc_uri"]) as proxy:
                now = datetime.datetime.now()
                returnval=proxy.appointment.getbydate(now.year,now.month,now.day,str(hashlib.sha256(self.Config["general"]["auth"].encode('utf-8')).hexdigest()))
                try:
                    valsJson = json.loads(returnval)
                    self.appointmentTable.setRowCount(len(valsJson))
                    i=0
                    for item in valsJson:
                        self.appointmentTable.setItem(i,0, QTableWidgetItem(item["appdatetime"]))
                        self.appointmentTable.setItem(i,1, QTableWidgetItem(item["appduration"]))
                        self.appointmentTable.setItem(i,2, QTableWidgetItem(item["appsubject"]))
                        i=i+1
                except json.decoder.JSONDecodeError:
                    print("Decoding JSON has failed: "+returnval)
                except:
                    print("General error")
                self.appointmentTable.resizeColumnsToContents()
        
            with xmlrpc.client.ServerProxy(self.Config["general"]["rpc_uri"]) as proxy:
                returnval=proxy.statuspicture.get(str(hashlib.sha256(self.Config["general"]["auth"].encode('utf-8')).hexdigest()))
                try:
                    image = Image.open(io.BytesIO(base64.b64decode(returnval)))
                    statuspic = self.pil2pixmap(image)
                    self.statusLabel.setPixmap(statuspic)
                    self.statusLabel.setScaledContents(True)
                except:
                    print("Picture Error")
        
            with xmlrpc.client.ServerProxy(self.Config["general"]["rpc_uri"]) as proxy:
                returnval=proxy.bundy.getweek(str(hashlib.sha256(self.Config["general"]["auth"].encode('utf-8')).hexdigest()))
                summarystring = ""
                try:
                    timesJson = json.loads(returnval)
                    for dateline in timesJson:
                        summarystring = summarystring + dateline["datdate"]+":\t"+dateline["datworktime"]+"\n"
                except json.decoder.JSONDecodeError:
                    print("Decoding JSON has failed: "+returnval)
                except:
                    print("General error")
                
                self.stundenLabel.setText(summarystring)
        except:
            self.setOption("misc","lasterror",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                
        self.setOption("misc","lastrun",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    def pil2pixmap(self, im):
        if(im.mode == "RGB"):
            r, g, b = im.split()
            im = Image.merge("RGB", (b, g, r))
        elif(im.mode == "RGBA"):
            r, g, b, a = im.split()
            im = Image.merge("RGBA", (b, g, r, a))
        elif (im.mode == "L"):
            im = im.convert("RGBA")
        im2 = im.convert("RGBA")
        data = im2.tobytes("raw", "RGBA")
        qim = QtGui.QImage(data, im.size[0], im.size[1], QtGui.QImage.Format_ARGB32)
        pixmap = QtGui.QPixmap.fromImage(qim)
        return pixmap


        



app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec_()
