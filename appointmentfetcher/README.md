# Get Outlook Appointments
This Script sends all Outlook appointments from current day (default, configurable) to the xmlrpc-server.
## Options
On first call, the script creates a configuration file. You can customize the following parameters:
- **auth**: String to athenticate yourself to the xmlrpc server
- **rpc_uri**: URI of the xlmrpc server
- **days**: daysyou would like to fetch with one run