import win32com.client
import datetime
import os
from collections import namedtuple
import psutil
import ssl
import xmlrpc.client
import configparser
import hashlib
 
event = namedtuple("event", "Start Subject Duration Body Organizer ReqAttendees OptAttendees")

def writeConfig(configObj,fileString):
    with open(fileString, 'w') as configfile:
        configObj.write(configfile)

def initConfig(settingsfile):
    Config=configparser.ConfigParser()
    Config.read(settingsfile)
    if(not Config.has_section("general")):
        Config.add_section("general")
        writeConfig(Config,settingsfile)
    if(not Config.has_option("general", "auth")):
        Config["general"]["auth"]="<enter authID>"
        writeConfig(Config,settingsfile)
    if(not Config.has_option("general", "rpc_uri")):
        Config["general"]["rpc_uri"]="rpc_uri"
        writeConfig(Config,settingsfile)
    if(not Config.has_option("general", "days")):
        Config["general"]["days"]=str(2)
        writeConfig(Config,settingsfile)
    return Config

def setOption(configObj,settingsfile,section,key,value):
    if(not configObj.has_section(section)):
        configObj.add_section(section)
    Config[section][key]=value
    writeConfig(configObj,settingsfile) 

def get_date(datestr):
    try:  # py3
        #See: https://docs.microsoft.com/en-us/previous-versions/office/developer/office-2010/ff869026(v=office.14)
        #There are currently three different start times, depends on used Timezone.
        #Only UTC Stuff works for me currently
#        adate = datetime.datetime.fromtimestamp(datestr.Start.timestamp())
        adate = datetime.datetime.fromtimestamp(datestr.StartUTC.timestamp())
#        adate = datetime.datetime.fromtimestamp(datestr.StartInStartTimeZone.timestamp())
    except Exception:
        adate = datetime.datetime.fromtimestamp(int(datestr.Start))
    return adate
 
 
def getCalendarEntries(days=4, dateformat="%d/%m/%Y"):
    """
    Returns calender entries for days default is 1
    Returns list of events
    """
    Outlook = win32com.client.Dispatch("Outlook.Application")
    ns = Outlook.GetNamespace("MAPI")
    appointments = ns.GetDefaultFolder(9).Items
    appointments.Sort("[Start]")
    appointments.IncludeRecurrences = "True"
    today = datetime.datetime.today()
    begin = today.date().strftime(dateformat)
    tomorrow = datetime.timedelta(days=days) + today
    end = tomorrow.date().strftime(dateformat)
    appointments = appointments.Restrict(
        "[Start] >= '" + begin + "' AND [END] <= '" + end + "'")
    events = []
    for a in appointments:
        adate = get_date(a)
        events.append(event(adate, a.Subject, a.Duration,a.Body,a.Organizer,a.RequiredAttendees,a.OptionalAttendees))
    return events
 
 
if __name__ == "__main__":
    settingsfile = os.path.dirname(
        os.path.realpath(__file__))+os.path.sep+"settings.ini"
    Config = initConfig(settingsfile)
    authID = Config.get("general", "auth")
    rpc_uri = Config.get("general", "rpc_uri")
    daystofetch=Config.getint("general", "days")
    if "outlook.exe" in (p.name().lower() for p in psutil.process_iter()):
        events = getCalendarEntries(daystofetch)
        context = ssl.SSLContext()
        for event in events:
            with xmlrpc.client.ServerProxy(rpc_uri,context=context) as proxy:
                print(event.Subject)
                returnval = proxy.appointment.add(event.Start.year, event.Start.month, event.Start.day,
                                                  event.Start.hour, event.Start.minute, event.Start.second,
                                                  event.Duration, event.Subject, event.Body, "Organizer: " + event.Organizer+"\nRequired: "+event.ReqAttendees+"\nOptional: "+event.OptAttendees,
                                                  1, os.path.basename(__file__), str(hashlib.sha256(authID.encode('utf-8')).hexdigest()))
                print(returnval)
    else:
        print("Outlook not running")
    setOption(Config,settingsfile,"misc","lastrun",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
