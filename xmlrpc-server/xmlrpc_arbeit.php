<?php

include "lib/xmlrpc.inc";     // from http://phpxmlrpc.sourceforge.net/
include "lib/xmlrpcs.inc";    // from http://phpxmlrpc.sourceforge.net/
include "dbfunc.inc.php";

// (function "mapping" is defined in  `new xmlrpc_server` constructor parameter.
function addappointment ($xmlrpcmsg) 
{
    $year = $xmlrpcmsg->getParam(0)->scalarval();
    $month = $xmlrpcmsg->getParam(1)->scalarval();
    $day = $xmlrpcmsg->getParam(2)->scalarval();
    $hour = $xmlrpcmsg->getParam(3)->scalarval();
    $minute = $xmlrpcmsg->getParam(4)->scalarval();
    $second = $xmlrpcmsg->getParam(5)->scalarval();
    $duration = $xmlrpcmsg->getParam(6)->scalarval();
    $subject = $xmlrpcmsg->getParam(7)->scalarval();
    $body = $xmlrpcmsg->getParam(8)->scalarval();
    $participants = $xmlrpcmsg->getParam(9)->scalarval();
    $severity = $xmlrpcmsg->getParam(10)->scalarval();
    $source = $xmlrpcmsg->getParam(11)->scalarval();
    $authID = $xmlrpcmsg->getParam(12)->scalarval();

    $conn = getMySQLConn ();

    if ($conn->connect_error) {
        $result = $conn->connect_errno;
    } else {
        if (checkAuth($authID)) {
            $appdate = $year . "-" . $month . "-" . $day . " " . $hour . ":" . $minute . ":" . $second;
            $sql = "INSERT INTO appointments (appdatetime, appduration, appsubject, appbody, appparticipants, appseverity, appsource)
        VALUES ('" . $appdate . "', '" . $duration . "', '" . $conn->real_escape_string($subject) . "', '" . $conn->real_escape_string($body) . "', '" . $conn->real_escape_string($participants) . "', '" . $severity . "', '" . $source . "');";

            if ($conn->query($sql) === TRUE) {
                $result = 0;
            } else {
                $result = $conn->errno;
            }
        } else {
            $result = 403;
        }
        $conn->close();
    }

    $xmlrpcretval = new xmlrpcval($result, "int"); // creating value object
    $xmlrpcresponse = new xmlrpcresp($xmlrpcretval); // creating response object

    return $xmlrpcresponse; // returning response
}

function getappointments ($xmlrpcmsg) 
{
    $year = $xmlrpcmsg->getParam(0)->scalarval();
    $month = $xmlrpcmsg->getParam(1)->scalarval();
    $day = $xmlrpcmsg->getParam(2)->scalarval();
    $authID = $xmlrpcmsg->getParam(3)->scalarval();

    $resultrows = array();
    
    // Create connection
    $conn = getMySQLConn ();
    // Check connection
    if ($conn->connect_error) {
        $resultrows[] = $conn->connect_errno;
    } else {
        if (checkAuth($authID)) {
            $appdate = $day . "-" . $month . "-" . $year;
            $dateobj = DateTime::createFromFormat('j-n-Y', $appdate);
            $sql = "SELECT DATE_FORMAT(appdatetime, '%d.%m.%Y %H:%i') as appdatetime, appduration, appsubject FROM appointments WHERE DATE( appdatetime ) = '" . $dateobj->format('Y-m-d') . "' order by appdatetime asc;";
            //        $resultrows[]=$sql;

            $resultset = $conn->query($sql);

            if ($resultset->num_rows > 0) {
                while ($row = $resultset->fetch_assoc()) {
                    $resultrows[] = $row;
                }
            }
            $resultset->close();
        } else {
            $resultrows[] = 403;
        }
        $conn->close();
    }

    $xmlrpcretval = new xmlrpcval(json_encode($resultrows), "string"); // creating value object
    $xmlrpcresponse = new xmlrpcresp($xmlrpcretval); // creating response object

    return $xmlrpcresponse; // returning response
}

function checkbundy ($xmlrpcmsg) 
{
    $bundytype = $xmlrpcmsg->getParam(0)->scalarval();
    $authID = $xmlrpcmsg->getParam(1)->scalarval();

    // Create connection
    $conn = getMySQLConn ();
    // Check connection
    if ($conn->connect_error) {
      $result = $conn->connect_errno;
    }
    else
    {
        if (checkAuth($authID))
        {
            $sql = "INSERT INTO bundy (type) VALUES ('".$bundytype."');";
        
            if ($conn->query($sql) === TRUE) {
                $result = 0;
            } else {
                $result = $conn->errno;
            }
    
            if($bundytype == "KOMMEN" || $bundytype == "STARTEN")
            {
                $sqlcome = "INSERT INTO bundytimes(bunleave, buninterval, bunsource, buncomment) VALUES(NULL, date_add(CURRENT_TIMESTAMP,interval 5 minute), NULL, '".$bundytype."');";
            
                if ($conn->query($sqlcome) === TRUE) {
                    $result = 0;
                } else {
                    $result = $conn->errno;
                }
            }
            elseif ($bundytype == "GEHEN" || $bundytype == "ENDE")
            {
                $sqlleave = "UPDATE bundytimes SET bunleave = CURRENT_TIMESTAMP, buncomment = CONCAT(buncomment, ', ".$bundytype."') ORDER BY buncome DESC LIMIT 1;";
            
                if ($conn->query($sqlleave) === TRUE) {
                    $result = 0;
                } else {
                    $result = $conn->errno;
                }
            }
            elseif ($bundytype == "INTERVALL")
            {
                $sqlleave = "UPDATE bundytimes SET buninterval = CURRENT_TIMESTAMP ORDER BY buncome DESC LIMIT 1;";
            
                if ($conn->query($sqlleave) === TRUE) {
                    $result = 0;
                } else {
                    $result = $conn->errno;
                }
            }
        }
        else
        {
            $result = 403;
        }
        $conn->close();
    }
    
    $xmlrpcretval = new xmlrpcval($result, "int"); // creating value object
    $xmlrpcresponse = new xmlrpcresp($xmlrpcretval); // creating response object

    return $xmlrpcresponse; // returning response
}

function getstatus ($xmlrpcmsg) 
{
    $authID = $xmlrpcmsg->getParam(0)->scalarval();
    $result="";

    // Create connection
    $conn = getMySQLConn ();
    // Check connection
    if ($conn->connect_error) {
        $result = $conn->connect_errno;
    }
    else
    {
        if (checkAuth($authID))
        {
            $sql = "select cststatusb64 from currStatus order by cstset desc limit 1";

            $resultset = $conn->query($sql);

            if ($resultset->num_rows > 0) {
                while ($row = $resultset->fetch_assoc()) {
                    $result = $row["cststatusb64"];
                }
            } else {
                $result = "0 results";
            }
           $resultset->close();
        }
        else
        {
            $result = 403;
        }
        $conn->close();
    }

    $xmlrpcretval = new xmlrpcval($result, "string"); // creating value object
    $xmlrpcresponse = new xmlrpcresp($xmlrpcretval); // creating response object

    return $xmlrpcresponse; // returning response
}

function setstatus ($xmlrpcmsg) 
{
    $statuspicture = $xmlrpcmsg->getParam(0)->scalarval();
    $desc = $xmlrpcmsg->getParam(1)->scalarval();
    $source = $xmlrpcmsg->getParam(2)->scalarval();
    $hash = $xmlrpcmsg->getParam(3)->scalarval();
    $authID = $xmlrpcmsg->getParam(4)->scalarval();
    $result=0;

    $conn = getMySQLConn ();
    if ($conn->connect_error) {
        $result = $conn->connect_errno;
    } else {
        if (checkAuth($authID)) {
            $sql = "select csthash from currStatus order by cstset desc limit 1";

            $resultset = $conn->query($sql);

            if ($resultset->num_rows > 0) {
                while ($row = $resultset->fetch_assoc()) {
                    $resulthash = $row["csthash"];
                }
            }
            
            if(strcmp($resulthash, $hash) !== 0)
            {
                $sql = "INSERT INTO currStatus(cststatusb64,cstdesc,cstsource,csthash) VALUES ('".$statuspicture."','".$desc."','".$source."','".$hash."');";
        
                if ($conn->query($sql) === TRUE) {
                    $result = 0;
                } else {
                    $result = $conn->errno;
                }
            }
        } else {
            $result = 403;
        }
        $conn->close();
    }

    $xmlrpcretval = new xmlrpcval($result, "int"); // creating value object
    $xmlrpcresponse = new xmlrpcresp($xmlrpcretval); // creating response object

    return $xmlrpcresponse; // returning response
}

function bundyweeksummary ($xmlrpcmsg) 
{
    $authID = $xmlrpcmsg->getParam(0)->scalarval();

    $resultrows = array();
    
    // Create connection
    $conn = getMySQLConn ();
    // Check connection
    if ($conn->connect_error) {
        $resultrows[] = $conn->connect_errno;
    }
    else
    {
        if (checkAuth($authID))
        {
            $sql = "SELECT DATE_FORMAT(datdate, '%d.%m.%Y') AS datdate, dathours, datminutes, concat(LPAD(dathours, 2, 0),':',LPAD(datminutes, 2, 0)) as datworktime FROM bundydatesum WHERE datdate >= first_day_of_week(now()) ORDER BY datdate ASC;";

            $resultset = $conn->query($sql);

            if ($resultset->num_rows > 0)
            {
                // output data of each row
                while($row = $resultset->fetch_assoc())
                {
                    $resultrows[] = $row;
                }
            }
            $resultset->close();
        }
        else
        {
            $resultrows[] = 403;
        }
        $conn->close();
    }

    $xmlrpcretval = new xmlrpcval(json_encode($resultrows), "string"); // creating value object
    $xmlrpcresponse = new xmlrpcresp($xmlrpcretval); // creating response object

    return $xmlrpcresponse; // returning response
}

// creating XML-RPC server object that handles 1 method
$s = new xmlrpc_server(
            array(
                "appointment.add" =>  array(
                    "function" => "addappointment",
                    "signature" => array(array($xmlrpcInt,$xmlrpcInt, $xmlrpcInt, $xmlrpcInt,$xmlrpcInt, $xmlrpcInt, $xmlrpcInt,$xmlrpcInt,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcInt,$xmlrpcString,$xmlrpcString)), // first is OUT, then comes in
                    "docstring" =>  'add appointment. (Returnval,Year,Month,Day,Hour,Minute,Second,duration,Subject,Body,Participants,Severity,Source,Auth)' // description
                    ) ,                    
                "appointment.getbydate" =>  array(
                    "function" => "getappointments",
                    "signature" => array(array($xmlrpcString,$xmlrpcInt,$xmlrpcInt,$xmlrpcInt,$xmlrpcString)), // first is OUT, then comes in
                    "docstring" =>  'get events of one day as json (year,month,day,auth)'
                    ),
                "bundy.check" =>  array(
                    "function" => "checkbundy",
                    "signature" => array(array($xmlrpcInt,$xmlrpcString,$xmlrpcString)), // first is OUT, then comes in
                    "docstring" =>  'add a time event (type,auth)'
                    ),
                "bundy.getweek" =>  array(
                    "function" => "bundyweeksummary",
                    "signature" => array(array($xmlrpcString,$xmlrpcString)), // first is OUT, then comes in
                    "docstring" =>  'get work times of current week(auth)'
                    ),
                "statuspicture.set" =>  array(
                    "function" => "setstatus",
                    "signature" => array(array($xmlrpcInt,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString)), // first is OUT, then comes in
                    "docstring" =>  'set status pictureadd a time event (picture as base64 string,desc,source,hash,auth)'
                ),
                "statuspicture.get" =>  array(
                        "function" => "getstatus",
                        "signature" => array(array($xmlrpcString,$xmlrpcString)), // first is OUT, then comes in
                        "docstring" =>  'set status pictureadd a time event (auth)'
                        )
            )
        );
?>
