<?php
function getMySQLConn()
{
    $servername = "<dbserver>";
    $username = "<dbuser>";
    $password = "<dbpass>";
    $dbname = "<dbname>";

    $retConn = new mysqli($servername, $username, $password, $dbname);

    $retConn->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

    return $retConn;
}

function checkAuth($authID2Check)
{
    $sqlauth = "SELECT * FROM authID WHERE autsid = sha2('" . $authID2Check . "',256);";
    $checkConn = getMySQLConn();
    if ($checkConn->connect_error) {
        return false;
    }
    $authresultset = $checkConn->query($sqlauth);
    $authRows = $authresultset->num_rows;
    $authresultset->close();
    $checkConn->close();
    if ($authRows > 0) {
        return true;
    }
    return false;
}
?>