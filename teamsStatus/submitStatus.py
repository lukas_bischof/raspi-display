#!/usr/bin/env python

import sys
import os
import ctypes as ct
from ctypes import wintypes as wt
from shutil import copyfile
from PIL import Image
import hashlib
import xmlrpc.client
import base64
import configparser
import datetime
import ssl

settingsfile = os.path.dirname(os.path.realpath(__file__))+os.path.sep+"settings.ini"

class ConfigManager:
    def __init__(self,filename=os.path.dirname(os.path.realpath(__file__))+os.path.sep+"settings.ini"):
        self.settingsfile=filename
        self.initConfig()

    def writeConfig(self):
        with open(self.settingsfile, 'w') as configfile:
            self.Config.write(configfile)

    def initConfig(self):
        self.Config=configparser.ConfigParser()
        self.Config.read(self.settingsfile)
        if(not self.Config.has_section("general")):
            self.Config.add_section("general")
            self.writeConfig()
        if(not self.Config.has_option("general", "auth")):
            self.Config["general"]["auth"]="<enter authID>"
            self.writeConfig()
        if(not self.Config.has_option("general", "rpc_uri")):
            self.Config["general"]["rpc_uri"]="rpc_uri"
            self.writeConfig()
        if(not self.Config.has_option("general", "logs_file")):
            self.Config["general"]["logs_file"]=os.getenv('appdata')+os.path.sep+"Microsoft"+os.path.sep+"Teams"+os.path.sep+"logs.txt"
            self.writeConfig()

    def setOption(self,section,key,value):
        if(not self.Config.has_section(section)):
            self.Config.add_section(section)
        self.Config[section][key]=value
        self.writeConfig()

configmanager=ConfigManager()

SGUPP_DIRECTORY = 0x01
SGUPP_DEFAULTDIRECTORY = 0x02
SGUPP_CREATEPICTURESDIR = 0x80000000

def hash_file(filename):
    h = hashlib.sha256()

    with open(filename,'rb') as file:
        chunk = 0
        while chunk != b'':
            chunk = file.read(1024)
            h.update(chunk)
    return h.hexdigest()


def returnresbystatus(_currstatus):
    statusdict = {"Available": ["#00FF00","available.png"],
                  "Away": ["#FFFF00","away.png"],
                  "BeRightBack": ["#FFFF00","away.png"],
                  "Busy": ["#FF0000","dnd2.png"],
                  "ConnectionError":[ "#585858","offline.png"],
                  "DoNotDisturb": ["#FF0000","dnd.png"],
                  "InAMeeting":[ "#FF0000","meeting.png"],
                  "NewActivity": ["#0000FF","new.png"],
                  "NoNetwork":[ "#585858","offline.png"],
                  "OnThePhone":[ "#FF0000","meeting.png"],
                  "Presenting": ["#FF0000","meeting.png"],
                  "AuthenticationFailed":[ "#585858","unknown.png"],
                  "undefined":[ "#585858","unknown.png"],
                  "Unknown":[ "#585858","unknown.png"]}
    try:
        return statusdict[_currstatus]
    except KeyError:
        return [ "#585858","error.png"]


def writeColorFile(_statusRes):
    statusfile = open(os.path.dirname(os.path.realpath(__file__))+os.path.sep+"teamsStatus.txt", "w")
    statusfile.write(_statusRes[0])
    statusfile.close()
    print(_statusRes[0])

def getTeamsStatus():
    logsfile = configmanager.Config.get("general", "logs_file")
    logfile = open(logsfile, "r")
    loglines = logfile.readlines()
    searchstring = " -> "
    endString = ")|"

    currStatus=""
    for logline in loglines:
        if " -- info -- StatusIndicatorStateService:" in logline and "(current state:" in logline:
            currStatus = logline[logline.rfind(searchstring)+len(searchstring):logline.rfind(endString)+len(endString)].strip()[:-2]
    return currStatus

def setStatusOverlay(imageFileName, teamsStatus):
    offset=8
    newFileName=os.path.dirname(os.path.realpath(__file__))+os.path.sep+"status.png"
    if os.path.exists(newFileName):
        os.remove(newFileName)
    statusImg = Image.open(imageFileName)
    size=96,96
    statusImg.thumbnail(size, Image.ANTIALIAS)
    statusImg.save(newFileName,"PNG")
    
    width, height = statusImg.size
    
    foreground = Image.open(os.path.dirname(os.path.realpath(__file__))+os.path.sep+"statusPics"+os.path.sep+teamsStatus)
    
    iconwidth,iconheight=foreground.size

    statusImg.paste(foreground, (width-(iconwidth+offset), height-(iconheight+offset)), foreground)
    statusImg.save(newFileName,"PNG")


    return newFileName

def encodeAndUploadImage(filename,status):
    authID = configmanager.Config.get("general", "auth")
    rpc_uri = configmanager.Config.get("general", "rpc_uri")

    sslContext = ssl.SSLContext()
    with xmlrpc.client.ServerProxy(rpc_uri,context=sslContext) as proxy:
        imgfile=open(filename,'rb')
        imgdata=imgfile.read()
        returnval = proxy.statuspicture.set(base64.b64encode(imgdata).decode("ascii"), status, os.path.basename(
            __file__), hash_file(filename), str(hashlib.sha256(authID.encode('utf-8')).hexdigest()))
        configmanager.setOption("misc","lastreturnval",str(returnval))
        configmanager.setOption("misc","lastrun",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

def getprofilepciturepath():
    shell32_dll = ct.WinDLL("shell32.dll")
    SHGetUserPicturePathW = shell32_dll[261]
    SHGetUserPicturePathW.argtypes = [wt.LPWSTR, wt.DWORD, wt.LPWSTR, wt.UINT]
    SHGetUserPicturePathW.restype = ct.c_long

    buf_len = 0xFF
    buf = ct.create_unicode_buffer(buf_len)
    flags = SGUPP_CREATEPICTURESDIR
    res = SHGetUserPicturePathW(None, flags, buf, buf_len)
    return buf.value

def main(*argv):
    profilepicturepath=getprofilepciturepath()
    teamsStatus=getTeamsStatus()
    writeColorFile(returnresbystatus(teamsStatus))
    mixedPictureFilename=setStatusOverlay(profilepicturepath,returnresbystatus(teamsStatus)[1])
    encodeAndUploadImage(mixedPictureFilename,teamsStatus)


if __name__ == "__main__":
    main(*sys.argv[1:])
