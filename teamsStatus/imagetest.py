#!/usr/bin/env python

import sys
import os
import ctypes as ct
from ctypes import wintypes as wt
from shutil import copyfile
from PIL import Image
import base64

with open("status.png",'rb') as img_file:
    data=img_file.read()
    datab64=base64.b64encode(data)

    imgtxt = open("imgtext.txt","wb")
    imgtxt.write(datab64)
    imgtxt.close()

ftxt = open("imgtext.txt","r")
ftxtdata = ftxt.read()
ftxt.close()

fh = open("imgtext.png","wb")
fh.write(base64.b64decode(ftxtdata))#.encode('ascii')))
fh.close()